

// ;(function(){
// 	'use strict'
// 	var data = []
// 	var template = $('#template').html()

// 	$('#form').find('button').on('click', function() {
// 		data.push($('#form').find('input').val())
// 		console.log(data)
		
// 		$('#form').find('.values').html('<li>' + data + '</li>')
// 	});


// })();

// =====================================

// var myMod = {
// 	nama: "Seno Setiaji",
// 	umur: "22th",
// 	profile: function(){
// 		alert(this.nama)
// 	},
// 	newProfile: function(newName){
// 		this.nama=newName;
// 	}
// }

// myMod.newProfile("Dinda P. Sapaah")
// myMod.profile();

;(function(){
	var modules ={
		data: ["rio"],
		// template: $('#template').html(),
		init: function(){
			this.dom();
			this.event();
		},
		
		dom: function(){
			this.$window = $(window)
			this.$el = $('#form');
			this.$button = this.$el.find('button');
			this.$input = this.$el.find('input');
			this.$ul = this.$el.find('ul');
			this.template = this.$el.find('#template').html();
			this.$edit = $('#editInput')
			this.editIndex =''

			// alert("Hallo John!")
		},

		event: function(){
			var btn, del;
			
			btn = this.$el
			
			this.$window.on('load', this.render())
			this.$ul.delegate('div.del', 'click', this.delete.bind(this));
			this.$ul.delegate('div.edit', 'click', this.edit.bind(this));
			this.$el.delegate('button#edit', 'click', this.editActions.bind(this));
			btn.on('submit', this.add.bind(this));


		},

		getVal: function(){
			var a = this.$input.val() 
			return a;
		},

		add: function(e){
			e.preventDefault()
			var input = this.getVal()
			this.data.push(input)
			this.$input.val(null)
			this.render()
			// console.log(this.data)
		},

		delete: function(el){
			var hapus = el.target.getAttribute('js-index')
			this.data.splice(hapus, 1)
			this.render()
			console.log(this.data)
		},

		edit: function(el){
			var edit = el.target.getAttribute('js-index')
			// this.$input.val(this.data[edit])
			this.$edit.val(this.data[edit])

			this.editIndex = edit
		},

		editActions:function(){
			this.data[this.editIndex] = this.$edit.val()
			this.render()
			console.log(this.$edit.val())
		},

		render: function(){
			var a;
			a = this.$ul

			a.html(' ')
			for(var i = 0; i < this.data.length; i++){
				a.append(`<li class="list d-flex" >` + this.data[i] + `
								<div class="edit" js-index="` + i + `">edit</div>
								<div class="del" js-index="` + i + `">&times;</div></li>`)
			}
		}

	};

	modules.init();

})();